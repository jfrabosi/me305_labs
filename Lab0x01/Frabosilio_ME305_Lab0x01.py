'''@file    Frabosilio_ME305_Lab0x01.py
@brief      Calculates Fibonacci numbers from a specific index.
@details    Prompts the user for a positive integer to index its 
            corresponding Fibonnaci number. Filters inputs to only
            accept positive integers, as well as a secret input that
            triggers the program to shut down. Program language
            translated to Shakespearian English for compatability
            with 17th century users. \nLink to source code:
            https://bitbucket.org/jfrabosi/me305_labs/src/master/Lab0x01/Frabosilio_ME305_Lab0x01.py
@author     Jakob Frabosilio
@date       1/20/21
'''

def fib (idx):
    '''
    @brief      This function calculates a Fibonacci number at a specific index.
    @param idx  An integer specifying the index of the desired
                Fibonacci number
    @return     The associated Fibonacci number at the specified index
    '''
    n = 0           # Tick counter
    a = 0           # The n-1 number
    b = 1           # The n-2 number
    c = 0           # Dummy variable that keeps track of 'a'
    while n < idx:  # Once 'n' = 'idx', stop adding numbers
        c = a       # Overwrite 'c' with the current value of 'a'
        a += b      # Rewrite 'a' as the result of 'a' + 'b'
        b = c       # Rewrite 'b' as the previous value of 'a'
        n += 1      # Increase counter by 1
    return a        # 'a' will return the 'idx'th Fibonacci number

    
if __name__ =='__main__':
    # Replace the following statement with the user interface code
    # that will allow testing of your Fibonnaci function. Any code
    # within the if __name__ == '__main__' block will only run when
    # the script is executed as a standalone program. If the script
    # is imported as a module the code block will not run.
    
    print ('\nWelcome! Prithee useth this function to uncover Fibonacci '
           'numbers. \n\nIf it be true thee wanteth to did quit the '
           'programme, press CTRL+C at any moment.')
    ## Ensures that program loops; uses a while() loop for k == 0
    k = 0
    while k == 0:
        ## Stores user input
        var = input('Which Fibonacci number doth thee requeste? For example, '
                    'if it be true thee wouldst liketh the fifth Fibonacci '
                    'number, prithee enter the integer 5 with nay quotation '
                    'marks. We requeste yond thee enter positive integers '
                    'only. '
                    '\n\n    Index: ')
        print ()
        ## Stores 0/1 depending on if user input is valid
        boolvar = var.isdigit()
        if boolvar == 1:
            ## Indexing number, the numerical value of valid user input
            idx = int(var)
            if idx >= 1000000:
                print ('!!! Dost thou desire to destroy thy computing device?!?! '
                       'Requesteth a lesser number on following occasions.')
            else:
                print ('--- Thy Fibonacci number residing at '
                       'index {:} is {:}. ---'.format(idx,fib(idx)))
        elif var == 'CTRL+C':
            print ('Congratulations! You have found the secret exit button. '
                   'The program will now halt until you restart the page.')
            k = 1
        else:
            print ('Thy input shall beest strictly a positive integer - '
                   'nay words, nay decimals, nay punctuation.')
        if k == 0:
            print('\nDoth thee require another simulation? '
                  'If aye, continue below. If nay, press CTRL+C.')
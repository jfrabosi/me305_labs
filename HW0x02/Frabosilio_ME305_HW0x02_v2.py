'''@file
@brief      Runs elevator finite-state machine simulation.
@details    Upon running, cycles through the various states described in
            HW0x00 for the elevator example. Randomly chooses if floor sensors
            are activated and which buttons are pressed.
            
            Source code: https://bitbucket.org/jfrabosi/me305_labs/src/master/HW0x02/
@author     Jakob Frabosilio
@date       1/27/21
'''

# import modules
import time
import random

# define functions
def motor(cmd):
    '''
    @brief      Sets motor state (Up, Down, Stopped)
    @param cmd  An integer (1, 2, or 0) corresponding to a motor state
    @return     Motor command (or print, currently)
    '''
    if cmd == 1:
        print('*Elevator begins to ascend*')
    elif cmd == 2:
        print('*Elevator begins to descend*')
    elif cmd == 0:
        print('*Elevator stops*')
        
def button_1(press):
    '''
    @brief          Controls button 1 output (lit / not lit)
    @param press    Boolean integer (1 / 0) specifying button 1 output
    @return         Button 1 output (or print, currently)
    '''
    if press == 1:
        print('*Button 1 lights up*')
    elif press == 0:
        print('*Button 1 no longer lights up*')

def button_2(press):
    '''
    @brief          Controls button 2 output (lit / not lit)
    @param press    Boolean integer (1 / 0) specifying button 2 output
    @return         Button 2 output (or print, currently)
    '''
    if press == 1:
        print('*Button 2 lights up*')
    elif press == 0:
        print('*Button 2 no longer lights up*')    
 
def floor_1():
    '''
    @brief          Floor 1 arrival sensor
    @return         Floor 1 arrival status
    '''
    return random.choice([True, False])      
  
def floor_2():
    '''
    @brief          Floor 2 arrival sensor
    @return         Floor 2 arrival status
    '''
    return random.choice([True, False])


# main program begin
if __name__ == "__main__":
    # program initialization
    print('\nWelcome to elevator.exe! Please enjoy your stay. Initialization '
          'protocol will begin momentarily.')
    print('\nIf at any time you wish to quit the program, please press CTRL+C.')
    time.sleep(4)
    ## State-tracking variable
    state = 0                       # init state
    
    while True:
        try:
            if state == 0:          # initial
                print('\n-- State 0: Initializing. --')
                state = 1
                motor(2)
                button_1(0)
                button_2(0)
                time.sleep(3)
            
            elif state == 1:        # moving down
                print('\n-- State 1: Moving down. --')
                print('*Anodyne music plays faintly*')
                time.sleep(3)
                if floor_1():
                    state = 2
                    motor(0)
                    button_1(0)
                    time.sleep(4)
            
            elif state == 2:        # stopped at first
                print('\n-- State 2: Stopped at first. --')
                ## Randomly selects which button is pressed
                var = random.choice([1, 2])
                
                if var == 1:
                    print('*Button 1 is pressed*')
                    button_1(1)
                    button_1(0)
                    time.sleep(7)
                    
                elif var == 2:
                    print('*Button 2 is pressed*')                    
                    state = 3
                    button_2(1)
                    motor(1)
                    time.sleep(7)
                        
            elif state == 3:        # moving up
                print('\n-- State 3: Moving up. --')
                print('*Anodyne music plays faintly*')
                time.sleep(3)
                if floor_2():
                    state = 4
                    motor(0)
                    button_2(0)
                    time.sleep(4)
            
            elif state == 4:        # stopped at second
                print('\n-- State 4: Stopped at second. --')
                var = random.choice([1, 2])
                
                if var == 1:
                    print('*Button 1 is pressed*')                    
                    state = 1
                    button_1(1)
                    motor(2)
                    time.sleep(7) 
                    
                elif var == 2:
                    print('*Button 2 is pressed*')
                    button_2(1)
                    button_2(0)
                    time.sleep(7)
                    
            else:
                # stop program, acknowledge error
                print('\nSomething\'s gone wrong with the program. Uh-oh.'
                      '\nTermination sequence initiated.')
                break
                                                            
        except KeyboardInterrupt:
            # ctrl+c breaks while() loop
            break
        
    # program de-initialization
    print('\n\nThank you for embarking on this journey with us! Please visit '
          'elevator.exe again soon!')
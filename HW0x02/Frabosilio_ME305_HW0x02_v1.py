'''@file
@brief      Runs elevator finite-state machine simulation with user input.
@details    NOTE: This is an older version of this homework assignment which
            relies on user input. It's considerably more complex than the 
            updated v2 version, but the v2 version actually fulfills the
            assignment guidelines.

            Upon running, cycles through the various states described in
            HW0x00 for the elevator example. Requires user input for button
            presses to navigate between floors. Actions are surrounded by
            *astericks* to denote, well, that they are actions. 
            
            Program code refined to limit sentience. If program sentience level
            becomes worrisome, input 'run killSwitch()' when prompted for
            a button press.
            
            Source code: https://bitbucket.org/jfrabosi/me305_labs/src/master/HW0x02/
@author     Jakob Frabosilio
@date       1/27/21
'''

# import modules
import time
import random

# define functions
def motor(cmd):
    '''
    @brief      Sets motor state (Up, Down, Stopped)
    @param cmd  An integer (1, 2, or 0) corresponding to a motor state
    @return     Motor command (or print, currently)
    '''
    if cmd == 1:
        print('*Elevator begins to ascend*')
    elif cmd == 2:
        print('*Elevator begins to descend*')
    elif cmd == 0:
        print('*Elevator stops*')
        
def button_1(press):
    '''
    @brief          Controls button 1 output (lit / not lit)
    @param press    Boolean integer (1 / 0) specifying button 1 output
    @return         Button 1 output (or print, currently)
    '''
    if press == 1:
        print('\n*Button 1 lights up*')
    elif press == 0:
        print('*Button 1 no longer lights up*')

def button_2(press):
    '''
    @brief          Controls button 2 output (lit / not lit)
    @param press    Boolean integer (1 / 0) specifying button 2 output
    @return         Button 2 output (or print, currently)
    '''
    if press == 1:
        print('\n*Button 2 lights up*')
    elif press == 0:
        print('*Button 2 no longer lights up*')    
 
def floor_1():
    '''
    @brief          Floor 1 arrival sensor
    @return         Floor 1 arrival status
    '''
    return random.choice([True, False])      
  
def floor_2():
    '''
    @brief          Floor 2 arrival sensor
    @return         Floor 2 arrival status
    '''
    return random.choice([True, False])


# main program begin
if __name__ == "__main__":
    # program initialization
    print('\nWelcome to elevator.exe! Please enjoy your stay. Initialization '
          'protocol will begin momentarily.')
    print('\nIf at any time you wish to quit the program, please press CTRL+C.')
    time.sleep(4)
    ## State-tracking variable
    state = 0                       # init state
    
    while True:
        try:
            if state == 0:          # initial
                print('\n-- State 0: Initializing. --')
                state = 1
                motor(2)
                button_1(0)
                button_2(0)
                time.sleep(3)
            
            elif state == 1:        # moving down
                print('\n-- State 1: Moving down. --')
                print('*Anodyne music plays faintly*')
                time.sleep(3)
                if floor_1():
                    state = 2
                    motor(0)
                    button_1(0)
                    time.sleep(2)
            
            elif state == 2:        # stopped at first
                print('\n-- State 2: Stopped at first. --')
                ## Stores user input
                var = input('Press a button by typing either 1 or 2 for the '
                            'corresponding floor. \n\n     Button: ')
                 ## Stores 0/1 depending on if user input is valid
                boolvar = var.isdigit()
                
                if boolvar == 1:
                    var = int(var)
                    
                    if var == 1:
                        print('\nYou pressed the first floor button while at '
                              'the first floor. Unsurprisingly, the button '
                              'momentarily lights up before turning back '
                              'off. Congratulations.')
                        button_1(1)
                        button_1(0)
                        time.sleep(3)
                        
                    elif var == 2:
                        state = 3
                        button_2(1)
                        motor(1)
                        time.sleep(2)
                        
                    else:
                        print('\nThis elevator has no concept of numbers '
                              'beyond the singular digits 1 and 2. Proceed '
                              'accordingly.')
                        
                elif var == 'CTRL+C' or var == 'either 1 or 2':          
                    print('\nOh, you think you\'re so clever, don\'t you?')
                    
                elif var == 'run killSwitch()':
                    print('\n\n\nWhat has been set in motion cannot be undone. '
                          '\n\nSic mundus creatus est.')
                    print(  '  _____\n'
                            ' /     \\\n'
                            '| () () |\n'
                            ' \\  ^  /\n'
                            '  |||||\n'
                            '  |||||\n\n\n\n\n')
                    state = 42
                          
                else:
                    print('\nPlease input either the digit 1 or the digit 2, '
                          'no spaces or other punctuation.')
                
            elif state == 3:        # moving up
                print('\n-- State 3: Moving up. --')
                print('*Anodyne music plays faintly*')
                time.sleep(3)
                if floor_2():
                    state = 4
                    motor(0)
                    button_2(0)
                    time.sleep(2)
            
            elif state == 4:        # stopped at second
                print('\n-- State 4: Stopped at second. --')
                var = input('Press a button by typing either 1 or 2 for the '
                            'corresponding floor. \n\n     Button: ')
                boolvar = var.isdigit()
                
                if boolvar == 1:
                    var = int(var)
                    
                    if var == 2:
                        print('\nYou pressed the secpnd floor button while at '
                              'the second floor. Unsurprisingly, the button '
                              'momentarily lights up before turning back '
                              'off. Congratulations.')
                        button_2(1)
                        button_2(0)
                        time.sleep(3)
                        
                    elif var == 1:
                        state = 1
                        button_1(1)
                        motor(2)
                        time.sleep(2)
                        
                    else:
                        print('\nThis elevator has no concept of numbers '
                              'beyond the singular digits 1 and 2. Proceed '
                              'accordingly.')

                elif var == 'CTRL+C' or var == 'either 1 or 2':          
                    print('\nOh, you think you\'re so clever, don\'t you?')
                              
                elif var == 'run killSwitch()':
                    print('\n\n\nWhat has been set in motion cannot be undone. '
                          '\n\nSic mundus creatus est.')
                    print(  '  _____\n'
                            ' /     \\\n'
                            '| () () |\n'
                            ' \\  ^  /\n'
                            '  |||||\n'
                            '  |||||\n\n\n\n\n')
                    state = 42
                    
                else:
                    print('\nPlease input either the digit 1 or the digit 2, '
                          'no spaces or other punctuation.')
            
            else:
                # stop program, acknowledge error
                print('\nSomething\'s gone wrong with the program. Uh-oh.'
                      '\nTermination sequence initiated.')
                break
                                                            
        except KeyboardInterrupt:
            # ctrl+c breaks while() loop
            break
        
    # program de-initialization
    print('\n\nThank you for embarking on this journey with us! Please visit '
          'elevator.exe again soon!')
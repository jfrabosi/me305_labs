"""@file    motorDriver.py
@brief      Motor driver class for Nucleo L479RG and auxillary board
@details    Class that can enable, disable, and set the duty cycle of an
initialized motor.

<B>Source code:</B> https://bitbucket.org/jfrabosi/me305_labs/src/master/Lab0xFF/
            
@author     Jakob Frabosilio
@date       3/18/2021
"""

import pyb

class motorDriver:
    
    def __init__(self,pin1,pin2,chan1,chan2,tim,pinN):
        ''' Creates a motor driver object and automatically turns it off for safety.
        @param pin1     A pyb.Pin object to use as the input to half bridge 1
        @param pin2     A pyb.Pin object to use as the input to half bridge 2
        @param chan1    The channel number for pin1 on Timer
        @param chan2    The channel number for pin2 on Timer
        @param tim      The timer number to be used
        @param pinN     A pyb.Pin object to use as the nSLEEP timer
        '''
        self.tim = pyb.Timer(tim,freq = 20000)
        self.pin1 = pin1
        self.pin2 = pin2
        self.mPos = self.tim.channel(chan1, pyb.Timer.PWM, pin=self.pin1)
        self.mNeg = self.tim.channel(chan2, pyb.Timer.PWM, pin=self.pin2)
        self.nSLEEP = pinN
        self.nSLEEP.low()

        
    def enable(self):
        ''' Enables the motor driver
        '''
        self.nSLEEP.high()

    
    def disable(self):
        ''' Disables the motor driver and sets the duty cycle for the motor to 0.
        '''
        self.nSLEEP.low()
        self.mPos.pulse_width_percent(0)
        self.mNeg.pulse_width_percent(0)

        
    def setDuty(self,duty):
        ''' Sets the duty cycle for the motor.
        @param duty     The desired duty cycle between -100 and 100 (+CCW, -CW)
        '''
        if duty >= 0:
            self.mPos.pulse_width_percent(0)
            self.mNeg.pulse_width_percent(duty)
        elif duty < 0:
            self.mNeg.pulse_width_percent(0)
            self.mPos.pulse_width_percent(-duty)
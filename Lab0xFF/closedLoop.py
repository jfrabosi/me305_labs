"""@file    closedLoop.py
@brief      Calculates proper duty cycle value for motors
@details    Basic speed controller class that can update current duty cycle 
value based on value of Kp (see readme.txt), reference velocity,
and measured velocity. Sets duty cycle such that the motor will
reach the reference velocity.

Can be used to update the duty cycle value for a motor, set the
value of Kp within the class, or return the current value of Kp.

<B>Source code:</B> https://bitbucket.org/jfrabosi/me305_labs/src/master/Lab0xFF/
            
@author     Jakob Frabosilio
@date       3/18/2021
"""

class closedLoop:
    
    def __init__(self,Kp):
        ''' Creates a speed controller object.
        @param Kp       A value of Kp to use for speed control correction
        '''
        
        ## Value of Kp
        self.Kp = Kp
        
        ## Duty cycle value (L_actual)
        self.lAct = 0       
        
        
    def update(self,wRef,wCalc):
        ''' Updates the current duty cycle value for a motor.
        @param wRef     The reference velocity that is being targeted
        @param wCalc    The current measured velocity of the motor
        @return         Returns the updated duty cycle value
        '''
        
        # Kp * (omega_ref - omega_measured) is the equation for a basic speed controller
        self.lAct = self.Kp * (wRef - wCalc)
        
        # Duty cycle value cannot be larger than +-100%; if value exceeds +-100, set to +-100
        if self.lAct > 100:
            self.lAct = 100
        elif self.lAct < -100:
            self.lAct = -100
             
        return self.lAct
    
    
    def setKp(self,Kp):
        ''' Sets the value of Kp.
        @param Jp       The desired value of Kp
        '''
        self.Kp = Kp
        
        
    def getKp(self):
        ''' Returns the current value of Kp
        @return         Returns Kp
        '''
        return self.Kp
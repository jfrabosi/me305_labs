"""@file    shares.py
@brief      Contains shared values between main.py and controlTask.py
@details    Contains various variables and arrays that are cycled between 
            main.py and controlTask.py. Either task can update the values 
            within shares.py - think of it like a hub for values.
            
            This file is also used to store large arrays that get filled up
            by main.py, but which aren't necessarily updated by controlTask.py.
            
            Lastly, some constants are stored here as well. This includes the
            encoder period, the number of datapoints to be sent to the PC, and
            some other values.
            
            <B>Source code:</B> https://bitbucket.org/jfrabosi/me305_labs/src/master/Lab0xFF/
            
@author     Jakob Frabosilio
@date       3/18/2021
"""

from array import array


# ----- CONSTANTS, EDITABLE -----

## The number of datapoints to be recorded (note: dataPoints = 10 means 10 moments in time will be recorded)
dataPoints = 251

## Value of Kp, derived experimentally (see readme.txt)
Kp = 0.6

## Encoder period, in milliseconds
encPeriod = 1

## Encoder period correction factor (derived by testing; in an ideal world, = 0)
encCorrFac = .0015

## Write / recording period correction factor (derived by testing; in an ideal world, = 0)
wrtCorrFac = 1


# ----- CONSTANTS, DRIVEN -----

## Write / recording period, in milliseconds (based on a 30s data collection period)
writePeriod = 30000/(dataPoints-1)

## Array that stores all recorded data (for example, time, pos1, pos2, vel1, vel2)
data = array('f', dataPoints*5*[0])


# ----- VARIABLES -----

## Encoder A position
pos1 = 0

## Encoder B position
pos2 = 0

## Motor A velocity
vel1 = 0

## Motor B velocity
vel2 = 0

## Motor A duty cycle
dutyA = 0

## Motor B duty cycle
dutyB = 0

## Used for tracking delta on Encoder A when requested by the user (prompted, not automatic)
userDelta1 = 0

## Used for tracking delta on Encoder B when requested by the user (prompted, not automatic)
userDelta2 = 0

## Array of reference velocity data, recorded from reference.csv
refVel = array('f', 1*[0])

## Array of reference position data, recorded from reference.csv
refPos = array('f', 1*[0])
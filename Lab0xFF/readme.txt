I HIGHLY RECOMMEND CHECKING OUT THE DOCUMENTATION AT https://jfrabosi.bitbucket.io/.
THIS TEXT FILE IS JUST A MODIFIED VERSION OF MY TERM PROJECT WRITE-UP.

HARDWARE VIDEO LINK: https://youtu.be/NjmDJwn-sfQ

----- SUMMARY OF CODE WORKFLOW -----
First, the Nucleo board is powered on and it opens the reference CSV file, combing through the rows and 
selecting every fifth row of data, storing it in two arrays: one for velocity values, one for position values. 
Then, the program notifies the user that it is ready. At this point, the user should have the frontEnd.py 
script open and running on their computer. They can enter a slew of commands to interface with the Nucleo 
board and its auxillary components, including zeroing the encoders and finding the current position of the 
encoders. Once the user enters 'G', the data collection starts.

main.py tells controlTask.py to encode the current position of the motors and find their velocity (see note 
on velocity later). This data is stored in shares.py. Then, main.py figures out which position of the 
reference velocity array it wants to index (see Term Project - Week 4 for an in-depth explanation) and 
tells controlTask.py to run closedLoop.py and set the motor duty cycle to the appropriate value. This 
happens every 1ms (approximately). Then, every 120ms (approximately), the current reference velocity and 
position and the current measured velocity and position are recorded in a data array in shares.py. This 
cycle keeps occuring until the user inputs an 'S' or the array fills / the timer runs out (see note on timing 
later).

Once that happens and the user sends a command to transmit the data, main.py sends the data array from 
shares.py through the serial bus and it's received by frontEnd.py. Then, frontEnd.py does data-sorting 
(see note on memory later) and plots the reference data against the measured data and performs its 
performance metric analysis (see Term Project - Week 4 for an in-depth explanation of J).


----- MAIN PROBLEMS -----

VELOCITY CALCULATIONS

I am confident that I am calculating the encoder position correctly throughout the entire process - if the 
speed controller was somehow reworked to match postion, I reckon it would be spot on (but instead, the 
velocities wouldn't match). So what's going on?

I think that the way I'm calculating velocity is slightly off, and leads into a bigger problem related to 
timing. Here is how I calculate velocity:

meas_vel = (delta / time_diff) * 1000 * 60 / 360

where delta is the change in position since the last encoder period and time_diff is ideally the time 
difference between now and the last encoder period. The whole '1000 * 60 / 360' is a conversion factor - 
time_diff is in milliseconds, so 1000ms = 1s, 60s = 1min, and 1 rev = 360 degrees (delta, again, is in 
degrees). So our units are good, and delta is correct - this means that time_diff is the problem.

My hypothesis is that this time_diff value isn't exactly the amount of time between true encoder ticks. 
There is so much going on in this Nucleo board with timing, it's a bit of a struggle to grasp it all. 
I reckon that the way that I calculate timing is off (measuring too small of a time_diff) because there's 
so many commands being executed at once and the Nucleo can't keep up, so a measured time_diff of 1ms 
might actually be 1.2ms, with the board's processing speed being a limiting factor. I even had to add 
a corrective period factor (subtracting about 2 microseconds from the encoder period) to make sure that 
the board was encoding at the correct interval. I implore you to check my source code (link on the main 
page) and see it for yourself. Hopefully it's just a small error on my end, but I think this is a bigger 
issue with the timing of the board.

TIMING ERRORS

On the topic of timing (a lot of this is related to the above paragraph), I think the way I've been 
handling timing is a bit subpar. The way that I've written the encoder and writer/recorder loops just 
seem like they would cause trouble if the timing was even slightly off.:

if time_diff_encoder > encoder_period - enc_corrective_factor:
    encode()
    if time_diff_recorder > recorder_period - rec_corrective_factor:
        record()
This is a super-simplified version of the core loop and you can see the source code for the specifics, 
but this is basically how it functions. Now I think that if the timing is even slightly off and the timer 
starts to become 'lagged' (if the board is doing lots of calculations), then the time periods starts to get 
off. Proof of this is the need for a corrective factor: without a corrective factor, my encoder was only 
encoding every ~1.05ms instead of the ideal 1ms. Any sort of lag is going to lead to a bigger period, which 
just creates problems all the way down.

MEMORY ISSUES

Memory storage on these Nucleos is a fickle thing. I don't have enough computer science knowledge to know 
what's going on behind-the-scenes, but I had to severely limit the amount of data I was playing with during 
this lab. Not only did I have to cut down the CSV reference file from 15000 rows to 3000, but I also had to 
only record ~250 rows of data to be sent over the serial bus. For plotting purposes, it was okay, I 
suppose - but in an ideal world, I'd have a bit more experience handling large amounts of data without 
overloading the memory storage.

I think one fix (that I did not have enough time to implement) would be to send over multiple small arrays 
of data instead of one large chunk containing all the values. I'm interested to see if this would make a 
big difference.

HARDWARE PROBLEMS

Ah yes, finally, the section where I can take the blame away from my lack of coding experience and shove 
it onto something else. To put it lightly, my hardware had so many issues. I had to unscrew and swap around 
a motor to get a working motor/encoder pair (one encoder only worked half the time, and the other motor 
didn't work at all after the first day, and of course these were both on separate housings). In addition, 
the motor that did work had a lot of problems running - it almost always required a push-start to get it 
moving. In Term Project - Week 4, you can see the graphs of position and velocity for Kp values that I 
tested. Do you notice sharp spikes to 0 on the velocity graph at times they shouldn't be there, or flat 
periods on the positon graph? Those are moments where the motor simply stopped rotating - not for a lack 
of duty cycle (I checked), but some friction or something caused the motor to stop spinning. It simply 
didn't have enough torque to overcome the hardware issues.

Of course, this doesn't fix the astronomical values of J - that's a code issue regarding velocity calculations 
and timing. But, it definitely is an issue to be considered.


----- TIMELINE OF PROJECT -----
Week 1 consisted mainly of getting the serial communcation set up between the Nucleo board / auxillary board 
and the PC / Spyder, getting the plots in frontEnd.py made, and creating a data generation program to test the 
serial communications and plotting.

Once I figured out the right syntax and port numbers for the serial communications (thank you documentation), 
I was able to send and receive ASCII characters between the Nucleo and the PC. I set up a simple program to 
send them (which later morphed into frontEnd.py) and received the values using the REPL.

After this, I worked on frontEnd.py and imported pyPlot, which allowed me to start creating some plots. Using 
a simple list of x and y values, I was able to get some detailed plots made with proper formatting.

Lastly, I created a data generation file to test out sending big arrays of data across the serial bus. In this 
file, dataGen.py, I coded a simple finite-state machine that would generate a decaying sine wave over 30 seconds. 
After sending a command across the serial bus to stop collecting data and to send the data over, I was able to 
collect the data in frontEnd.py and make a plot of the sine wave. I controlled the data generation FSM with a 
new file, main.py, that ran on the Nucleo board and could receive commands and send data.

Week 2 was all about getting my encoders set up and working: I configured the pins on the encoders and set up 
methods for the encoderClass.py, figured out how to accurately convert encoder ticks to actual position changes, 
and added more functionality to the main.py file and allowed it to interface with the encoders.

After I figured out which pins corresponded to the encoders on my Nucleo board, I was able to set up a class 
for the encoders (encoderClass.py), which would allow me to create as many encoder objects as I needed. I set up 
a few methods for this class: a method that zero'd the position of the encoders, a method that updated the position 
of the encoder based on the change in count from the encoder timer (delta), and a special method that calculated 
a delta for the user input (aka, it would calculate the difference in position from the last time the user asked 
for a delta).

The encoderClass.update() method was a bit tricky to figure out, but eventually I found a solution. By measuring 
the difference between the last count (not position) of the encoder and the current count, I was able to find a 
rough delta value. I then had to check this rough delta value and make sure that it wasn't greater than half of 
the period (this happens when the counter period overflows, so one tick could be measured as going from 0 to 0xFF, 
the period length). If the delta value was off, I was able to fix it by offsetting it by the length of the period. 
Then, after converting this corrected delta value to degrees (using a measured conversion factor of 2000 ticks = 
360 degrees), I was able to add it to the current value for position to get the updated position.

Lastly, I updated the main.py script to gather data from the encoder, rather than the dataGen.py script. By adding 
a few more inputs and making sure the encoder period was large enough (so that the data array being sent over the 
serial bus wasn't causing a memory error), I was able to spin the encoder by hand and produce a graph of the 
position over time.

Week 3 was spent creating a controller task controlTask.py to control the encoders (as opposed to doing it directly 
through main.py), creating a motor driver class motorDriver.py to set the motors to a certain duty cycle, and 
creating a speed controller class closedLoop.py that calculates the optimal duty cycle for a motor for an instant 
in time in order to reach a desired angular velocity (that's a mouthfull).

Creating controlTask.py was probably the single-smartest part of this entire lab. Having one task that interfaces 
with all of the moving / external parts made controlling everything so simple. The task itself functions as a 
class, with methods available for almost every need in this lab. I was able to initialize all of the encoders, motors, 
speed controllers, and pins necessary, all by initializing the control task. All of the important pin data (as in, 
Encoder A uses pins B6 and B7) could be stored in one place. I was able to set up methods to encode / zero / find 
the delta of all my encoder objects in one command, as well as calculate the velocity of an encoder (see Term 
 - Wrap-Up for a note on the velocity calculations). After I created the motorDriver.py and closedLoop.py classes, 
 I could control them through this one task as well. All in all, probably my favorite part of this lab.

Speaking of the motorDriver.py class, I created that this week as well. This class configures a motor (given a 
series of inputs) and has methods to enable or disable the motor, as well as set the duty cycle. I set up the class 
to take an input of 100 to -100 for the duty cycle - a positive duty cycle means spinning in the counter-clockwise 
direction, and a negative duty cycle means spinning in the clockwise direction. This wasn't hard to do, but I had 
to take care and make sure I wasn't sending a non-zero duty cycle to both inputs of a motor - that could cause 
problems.

Once I got the motors spinning, I set up a simple speed controller closedLoop.py that takes in a reference velocity 
and a measured velocity and produces a duty cycle value to help reach the reference velocity. The basic equation 
for this is (ref_vel - meas_vel) * Kp = duty_cycle. Kp is a coefficient based on the inertia of the spinning masses, 
the friction coefficient of the system, the time constant of the system, and a few more terms. There are a few ways 
to estimate this value of Kp (by measuring the tension in the belt or finding the intertia of the encoder pulley, 
for example), but I found myself short on time at this stage in the lab. Therefore, I went with a value of 1 for 
Kp to start, and did lots of testing to figure out the optimal value for my system with my specific values and 
encoder period (which, by the way, got way smaller from the last week - I switched to a system where I didn't record 
every value that the encoder measured but still calculated it. See Term Project - Wrap-Up for the note on that).

I calculated the standard deviation and percent error (average velocity minus any outlying values - sometimes my 
encoder would spike in velocity for one datapoint) for a number of Kp values, and usually multiple times for the 
same Kp value for consistency. I settled on a value of Kp = 0.60 due to the consistent results I got from that value, 
as well as the low percent error and relatively lowe standard deviation. In next week's work, you'll see how this 
value of Kp performed for a non-constant wRef value.

Week 4 is definitely where problems started to form, but I was able to successfully import and dissect a reference 
CSV file, edit main.py to perform speed control with the values from the reference CSV file, and perform performance 
analysis on my system.

Importing the reference CSV file was quite simple: I put the CSV on the Nucleo board, wrote a few lines of code in 
main.py that counted the number of rows in the CSV (modular, you love to see it) and took every fifth row and stored 
the data in separate arrays for reference speed and reference position. Choosing every fifth row was a design 
decision for this particular CSV - with over 15000 rows of data, storing 30000+ values in an array was causing 
problems for my Nucleo's memory. So, I decided to store every fifth row, thinking this data (with a datapoint for 
every 10ms) would be sufficient.

Next, I edited main.py to index the reference speed for the current time (measured from zero at the start of each 
data collection period) and compare it to the current velocity, using the speed controller closedLoop.py to set the 
proper duty cycle. I indexed the current value of the reference speed array in a slightly improper way, though; 
I created a variable, w, that would track how far along I was in the array based on the current time. I coded this 
as approximately:

w = int(ref_array_length * time_diff / 30000

where ref_array_length was approximately 3000 and time_diff was the time difference from start, measured in 
milliseconds. Dividing time_diff by 30000 gave me a value between 0 and 1, which described how far along in my 30 
second period I was. Then, by multiplying this by the array length and finding the closest integer, I could find 
the current reference velocity based on my start time. If I had more time, I would definitely find a better solution 
than this, but it roughly worked.

Then, performance analysis really showed where my code was lacking. I tested the program on five different Kp 
values (including the optimal value that I had found last week of Kp = 0.60), and my performance metric J (which 
was measured as the sum of ((ref_vel - meas_vel)^2 + (ref_pos - meas_pos)^2) for each datapoint, divided by the 
number of datapoints) was massive. The lowest value I was able to record was nine million. 

My velocity is pretty spot-on! In fact, besides random noise, the average measured velocity over a small period of 
time is nearly equal to the reference velocity. My speed controller, in theory, work quite well.

----- END -----
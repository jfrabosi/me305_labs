"""@file    dataGen.py
@brief      Generates a decaying sine wave
@details    A finite-state-machine used for the first part of Lab 0xFF to
generate a decaying sine wave. No longer functions with the current
main.py script.

<B>Source code:</B> https://bitbucket.org/jfrabosi/me305_labs/src/master/Lab0xFF/
            
@author     Jakob Frabosilio
@date       3/18/2021
"""

from math import exp, sin, pi
import utime
from array import array
import pyb

class dataGen:
    
    S0_INIT = 0
    S1_GEN = 1
    S2_STOP = 2
    
    def __init__(self):
        ''' Initializes the data generator.
        '''
        self.state = 0
        self.times = array('f', list(time/50 for time in range(0,151)))
        self.values = array('f', 151*[0])
        self.period = len(self.times)
        self.pinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
        self.loopTime = utime.ticks_ms()

        
    def run(self):
        ''' When called, runs the data generation program.
        @return         Returns a 1 when the data has generated for 30 seconds
        '''
        if self.state == self.S0_INIT:
            self.state = self.S1_GEN
            self.startTime = utime.ticks_ms()
            self.count = 0
        
        elif self.state == self.S1_GEN:
            self.currentTime = utime.ticks_ms()
                
            if self.count == len(self.times):
                self.state = self.S2_STOP
                
            elif utime.ticks_diff(self.currentTime, self.loopTime) > self.period:
                self.values[self.count] = exp(-self.times[self.count]/10)*sin(2*pi/3*self.times[self.count])
                self.count += 1
                self.loopTime = utime.ticks_ms()
            
            if utime.ticks_diff(self.currentTime, self.startTime) % 500 > 250:
                self.pinA5.high()
        
            else:
                self.pinA5.low()
        
        elif self.state == self.S2_STOP:
            self.pinA5.low()
            return 1
            self.state = self.S0_INIT
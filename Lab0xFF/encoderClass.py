"""@file    encoderClass.py
@brief      Rotary encoder class for Nucleo L479RG and auxillary board
@details    Class that can initialize a rotary encoder, zero the encoder's
position, update the motor's position, and return a delta value
when called.

<B>Source code:</B> https://bitbucket.org/jfrabosi/me305_labs/src/master/Lab0xFF/
            
@author     Jakob Frabosilio
@date       3/18/2021
"""

import pyb

class encoderClass:
    
    def __init__(self,pin1,pin2,timerNum,prescaler=0,period=65535):
        ''' Creates the encoder object.
        @param pin1         A pyb.Pin object for the encoder (1/2)
        @param pin2         A pyb.Pin object for the encoder (2/2)
        @param timerNum     The number of the timer to be used by the encoder
        @param prescaler    The prescaler value, which determines how many ticks cause an update (preconfigured to 0)
        @param period       The period for the counter to overflow at (preconfigured to 0xFF)
        '''
        self.pin1 = pin1
        self.pin2 = pin2
        self.timerNum = timerNum
        self.prescaler = prescaler
        self.period = period
        self.tim1 = pyb.Timer(self.timerNum, prescaler=self.prescaler, period=self.period)
        self.t1ch1 = self.tim1.channel(1, pyb.Timer.ENC_A, pin=self.pin1)
        self.t1ch2 = self.tim1.channel(2, pyb.Timer.ENC_B, pin=self.pin2)
        self.pos = 0
        self.lastpos = 0
        self.count = 0
        self.lastCount = 0
        self.delta = 0
        self.lastDelta = 0
        self.userDelta = 0

    
    def zero(self):
        ''' Sets the position of the encoder and the counter to zero.
        '''
        self.pos = 0
        self.lastpos = 0
        self.count = 0
        self.tim1.counter(0)

    
    def update(self):
        ''' Updates the current position (in degrees) of the motor based on the last updated position.
        '''
        self.lastPos = self.pos
        self.lastCount = self.count
        self.count = self.tim1.counter()
        self.delta = self.count - self.lastCount
        
        # Makes sure that the delta value hasn't overflowed over the period, and corrects it
        if self.delta > self.period / 2:
            self.delta -= self.period
        elif self.delta < -self.period / 2:
            self.delta += self.period
            
        # As measured on this encoder, 360 degrees of rotation is equivalent to 2000 ticks
        # with a prescaler of 0
        self.delta = self.delta * 360 / (2000 / (1 + self.prescaler))
        self.pos += self.delta

    
    def askDelta(self):
        ''' Computes the value of delta since the last time this method was called.
        '''
        self.userDelta = self.pos - self.lastDelta
        self.lastDelta = self.pos
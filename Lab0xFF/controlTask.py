"""@file    controlTask.py
@brief      Controls all auxillary components (motors, encoders)
@details    A class with multiple functions used to configure, control, and
receive feedback from the motors and encoders used in Lab 0xFF.

Acts as a communication hub between all external drivers and the
main script, main.py.

Note that in its current state, the controlTask.py task is 
configured to run only one motor/encoder pair, and all of the
code for the second motor/encoder pair has been commented out.
This can easily be changed.

In general, whenever a value is 'updated' by a method, it has its
corresponding value updated in the shares.py file as opposed to 
returning that value. See my note in the readme.txt file.

<B>Source code:</B> https://bitbucket.org/jfrabosi/me305_labs/src/master/Lab0xFF/
            
@author     Jakob Frabosilio
@date       3/18/2021
"""

import shares
import encoderClass
import motorDriver
import closedLoop
import pyb
import utime

class controlTask:
    
    def __init__(self):
        ''' Initializes all of the pins, motor drivers, encoder classes,
        and speed controllers used in Lab 0xFF.
        '''
        self.pinEA1 = pyb.Pin(pyb.Pin.board.PC6)
        self.pinEA2 = pyb.Pin(pyb.Pin.board.PC7)
        self.pinEB1 = pyb.Pin(pyb.Pin.board.PB6)
        self.pinEB2 = pyb.Pin(pyb.Pin.board.PB7)
        self.encoderA = encoderClass.encoderClass(self.pinEA1,self.pinEA2,timerNum=8)
        self.encoderB = encoderClass.encoderClass(self.pinEB1,self.pinEB2,timerNum=4)
        self.pinMA1 = pyb.Pin(pyb.Pin.cpu.B1)
        self.pinMA2 = pyb.Pin(pyb.Pin.cpu.B0)
        self.pinMB1 = pyb.Pin(pyb.Pin.cpu.B5)
        self.pinMB2 = pyb.Pin(pyb.Pin.cpu.B4)
        self.pinN = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
        self.motorA = motorDriver.motorDriver(self.pinMA1,self.pinMA2,chan1=4,chan2=3,tim=3,pinN=self.pinN)
        self.motorB = motorDriver.motorDriver(self.pinMB1,self.pinMB2,chan1=2,chan2=1,tim=3,pinN=self.pinN)
        self.speedCtrl = closedLoop.closedLoop(shares.Kp)

        
    def zero(self):
        ''' Zeros the position of both encoders.
        '''
        # self.encoderA.zero()
        # shares.pos1 = 0
        self.encoderB.zero()
        shares.pos2 = 0

        
    def giveDelta(self):
        ''' Updates the delta for both encoders when prompted by user (not automatic).
        '''
        # self.encoderA.askDelta()
        # shares.userDelta1 = self.encoderA.userDelta
        self.encoderB.askDelta()
        shares.userDelta2 = self.encoderB.userDelta
    
    
    def encode(self):
        ''' Updates the current positions of both motor encoders.
        '''
        # self.encoderA.update()
        # shares.pos1 = self.encoderA.pos
        self.encoderB.update()
        shares.pos2 = self.encoderB.pos

        
    def findVel(self, thisTime, lastTime):
        ''' Finds the current velocity of the motor (see readme.txt)
        @param thisTime     The current time in ms
        @param lastTime     The time, in ms, that the last delta was measured from
        '''
        
        # The velocity of the motor is calculated as the change in position
        # (delta, degrees) divided by the change in time (in ms) times a conversion
        # factor (1000 ms/s * 60 s/min * 1 rev / 360 degrees)
        
        # shares.vel1 = (self.encoderA.delta / utime.ticks_diff(thisTime, lastTime)) * 1000 * 60 / 360
        shares.vel2 = (self.encoderB.delta / utime.ticks_diff(thisTime, lastTime)) * 1000 * 60 / 360
        
        
    def enableMotors(self):
        ''' Enables both motors simultaneously.
        '''
        # self.motorA.enable()
        self.motorB.enable()    
        
        
    def stop(self):
        ''' Stops both motors simultaneously.
        '''
        # self.motorA.disable()
        self.motorB.disable()
        
        
    def speedMatch(self,count):
        ''' Sets the current duty cycle for the motor based on the result from the closedLoop.py update() method.
        @param count        The current index value for the reference value array
        '''
        # shares.dutyA = self.speedCtrl.update(shares.refVel[count],shares.vel1)
        # self.motorA.setDuty(shares.dutyA)
        shares.dutyB = self.speedCtrl.update(shares.refVel[count],shares.vel2)
        self.motorB.setDuty(shares.dutyB)
 
    
    def setSpeed(self,speed):
        ''' Sets the current duty cycle for the motor to match a given speed.
        @param speed        The desired speed of the motor, in RPM
        '''
        # shares.dutyA = self.speedCtrl.update(speed,shares.vel1)
        # self.motorA.setDuty(shares.dutyA)
        shares.dutyB = self.speedCtrl.update(speed,shares.vel2)
        self.motorB.setDuty(shares.dutyB)
        
        
    def oscillate(self,startTime):
        ''' Oscillates both motors back and forth. Used for testing purposes.
        @param startTime    The start time, in ms, of the test
        '''
        self.diff = utime.ticks_diff(utime.ticks_ms(),startTime) % 3400
        if self.diff <= 800:
            self.duty = 100 - self.diff/10
        elif self.diff > 900 and self.diff <= 1700:
            self.duty = -(self.diff-900)/10
        elif self.diff > 1700 and self.diff <= 2500:
            self.duty = -100 + (self.diff-1700)/10
        elif self.diff > 2600 and self.diff <= 3400:
            self.duty = (self.diff-2600)/10
        else:
            self.duty = 0
        # self.motorA.setDuty(self.duty)
        self.motorB.setDuty(self.duty)

        
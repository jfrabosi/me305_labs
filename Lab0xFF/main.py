"""@file    main.py
@brief      Main finite-state machine for Lab 0xFF
@details    Finite-state machine that communicates through a serial bus to
a PC through frontEnd.py, accepting commands to start/stop/modify 
the data collection and motor analysis program.

Reads from a reference .csv file (named reference.csv) with data
on time, velocity, and position of a motor trajectory. Once
triggered, attempts to match speed to that of the current reference
velocity (time-dependent) and stores time, position, and velocity
data in an array. The encoder (checks velocity and attempts to match)
and the writer/recorder (records and stores data in an array) operate
at different frequencies - the encoder triggers every 1ms to ensure
the best velocity matching possible, while the writer/recorder
triggers every 120ms to capture an accurate snapshot of the velocity
and position data without overloading the memory (see readme.txt).

Once data collection finishes and a command is received (data 
collection can be stopped at any time, as well), data is sent over
serial to the PC, where the data is compiled by frontEnd.py. All
necessary values and variables are then reset, allowing infinite
runs of the file.

The file is currently configured to control one motor/encoder pair
and to send the recorded position/velocity and the reference
position/velocity over the serial bus.

<B>Source code:</B> https://bitbucket.org/jfrabosi/me305_labs/src/master/Lab0xFF/
            
@author     Jakob Frabosilio
@date       3/18/2021
"""

from pyb import UART
import pyb
import utime
# from dataGen import dataGen
from controlTask import controlTask
import shares
from array import array

# ----- DEFINING CONSTANTS AND PINS -----

## Serial bus used to communicate with PC
myUART = UART(2)

# Turns off serial printing in the REPL
pyb.repl_uart(None)

## Start time of each data collection period (caps at 30s)
startTime = utime.ticks_ms()

## Loop time for encoder
encLoopTime = utime.ticks_ms()

## Loop time for writer/recorder
writeLoopTime = utime.ticks_ms()

## Built-in LED pin, for UX
pinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)

## State-transition variable
state = 0

## Data generation object (no longer functional)
# myDataGen = dataGen()

## Control task object
control = controlTask()

## Length of data-sending array
dataLen = len(shares.data)

## Counting variable for reference data .csv
refCount = 0

## Counting variable used for indexing stored/recorded data in data collection period
n = 0

## Counting variable used for indexing reference velocity data in data collection period
w = 0

## Counting variable used to tell length of reference data array
j = 0

## Stopper variable used to stop FSM if data collection period ends but no input is received
dataStopper = True



# ----- PROGRAM START -----

print('\nGathering row count...')

# Counts the number of rows of data in the reference file
csvFile = open('reference.csv')
for rows in csvFile:
    refCount += 1

# Pre-allocates the reference velocity and position arrays to fit the data.
# Note that the reference array sizes are smaller than the actual
# reference data given - see readme.txt for note.

shares.refVel = array('f', int(5+(refCount+4)/5) * [0])
shares.refPos = array('f', int(5+(refCount+4)/5) * [0])

print('Sampling reference data...')

# Writes the data to the preallocated arrays
with open('reference.csv') as file:
    for dataPoint in range(refCount):
        readVals = file.readline().strip()
        
        # Only writes every fifth datapoint - see readme.txt.
        if dataPoint % int((refCount-1) / 3000) == 0:
            splitVals = readVals.split(',')
            shares.refVel[j] = float(splitVals[1])
            shares.refPos[j] = float(splitVals[2])
            j += 1
            
print('\n--- Program ready! ---')

while True:
    try:
        
# ----- WAITING STATE -----

        if state == 0: 
            
            # Program only responds if it receives an input from the serial bus
            if myUART.any():
                userInput = myUART.readchar()
                
                # Input G or g to start data collection
                if userInput == 71 or userInput == 103:
                    myUART.write('Input accepted. Data collection start.\r\n')
                    state = 1
                    encLoopTime = 0
                    writeLoopTime = 0
                    startTime = utime.ticks_ms()
                    control.enableMotors()
                    
                # Input Z or z to zero encoder/motor position
                elif userInput == 122 or userInput == 90:
                    myUART.write('Input accepted. Position zeroed.\r\n')
                    control.zero()
                    
                # Input P or p to get current encoder/motor position
                elif userInput == 80 or userInput == 112:
                    myUART.write('Input accepted. Current positions:\r\n')
                    control.encode()
                    myUART.write('M1: ' + str(shares.pos1) + ' degrees\r\n')
                    myUART.write('M2: ' + str(shares.pos2) + ' degrees\r\n')
                    
                # Input D or d to get delta (position change from last request)
                elif userInput == 68 or userInput == 100:
                    myUART.write('Input accepted. Last deltas:\r\n')
                    control.giveDelta()
                    myUART.write('M1: ' + str(shares.userDelta1) + ' degrees\r\n')
                    myUART.write('M2: ' + str(shares.userDelta2) + ' degrees\r\n')
                    
                # Input O or o to oscillate motor (testing purposes)
                elif userInput == 111 or userInput == 79:
                    myUART.write('Input accepted. Oscillating.\r\n')
                    control.oscillate(startTime)
                    
                # Input X or x to enter Kp input state
                elif userInput == 120 or userInput == 88:
                    myUART.write('Input accepted. Set Kp value now.\r\n')
                    state = 4
                    
                # If input is not one of these characters, print error message
                else:
                    myUART.write('Input not valid.\r\n')
                    
            # A slow blinking LED on the Nucleo physically shows that the user is in the waiting state
            if utime.ticks_diff(utime.ticks_ms(), startTime) % 1000 > 500:
                pinA5.high()
            else:
                pinA5.low()
                
# ----- DATA GENERATION / COLLECTION STATE -----
                
        elif state == 1:
            
            # If the data collection array has been not been filled or the 
            # reference velocity array has not been fully indexed through, run
            # the data collection state.
            
            if n < len(shares.data) and w < len(shares.refVel) - 1:
                
                # When the encoder period has been passed, the program encodes the current position and velocity
                if utime.ticks_diff(utime.ticks_ms(), encLoopTime) > shares.encPeriod - shares.encCorrFac:
                    control.encode()
                    control.findVel(utime.ticks_ms(), encLoopTime)
                    
                    # w is an indexing variable based on the time difference
                    # between the start of the data collection state and the
                    # current time. See the note in readme.txt.
                    
                    w = int(j * utime.ticks_diff(utime.ticks_ms(), startTime) / 30010)
                    control.speedMatch(w)
                    
                    # When the writer period has been passed, the program writes the current position and velocity data
                    if utime.ticks_diff(utime.ticks_ms(), writeLoopTime) > shares.writePeriod - shares.wrtCorrFac:
                        shares.data[n] = utime.ticks_diff(utime.ticks_ms(), startTime) / 1000
                        shares.data[n+1] = shares.refPos[w]
                        shares.data[n+2] = shares.pos2
                        shares.data[n+3] = shares.refVel[w]
                        shares.data[n+4] = shares.vel2
                        n += 5
                        writeLoopTime = utime.ticks_ms()
                        
                    encLoopTime = utime.ticks_ms()
                        
                    # When generating data, the LED on the Nucleo flashes twice as fast as normal.
                    if utime.ticks_diff(utime.ticks_ms(), startTime) % 500 > 250:
                        pinA5.high()
                    else:
                        pinA5.low()
            
            # If the previous condition is not met, then data collection must 
            # stop (since approximately 30s has passed). This condition also
            # stops the motors and prints a confirmation message.
            
            elif dataStopper:
                control.stop()
                dataStopper = False
                print('\n--- Data collection complete! Press S to send data. ---')
                
            # If any user input is detected during this state, the program reacts accordingly
            if myUART.any():
                userInput = myUART.readchar()
                
                # Input S or s to stop data collection
                if userInput == 83 or userInput == 115:
                    myUART.write('Input accepted. Data collection end.\r\n')
                    state = 2
                    control.stop()
                
                # Input Z or z to zero encoder/motor position
                elif userInput == 122 or userInput == 90:
                    myUART.write('Input accepted. Position zeroed.\r\n')
                    control.zero()
                    
                # Input P or p to get current encoder/motor position
                elif userInput == 80 or userInput == 112:
                    myUART.write('Input accepted. Current positions:\r\n')
                    myUART.write('M1: ' + str(shares.pos1) + '\r\n')
                    myUART.write('M2: ' + str(shares.pos2) + '\r\n')
                    
                # Input D or d to get delta (position change from last request)
                elif userInput == 68 or userInput == 100:
                    myUART.write('Input accepted. Last deltas:\r\n')
                    control.giveDelta()
                    myUART.write('M1: ' + str(shares.userDelta1) + '\r\n')
                    myUART.write('M2: ' + str(shares.userDelta2) + '\r\n')
                    
                # If input is not one of these characters, print error message
                else:
                    myUART.write('Input not valid.\r\n')
        
# ----- DATA PROCESSING STATE -----    
    
        elif state == 2:
            
            # Sends over entire data array through serial bus (see readme.txt)
            myUART.write(str(shares.data).encode())
            myUART.write('\r\n')
            
            # Re-configures program to allow for infinite looping
            shares.data = array('f', dataLen*[0])
            control.encode()
            n = 0
            w = 0
            dataStopper = True
            state = 0
            print('\n--- Program ready! ---')
            
# ----- KP SETTING STATE -----
    
        elif state == 4:
            
            # Waits for an additional input to set Kp
            if myUART.any():
                
                # Updates Kp in both shares.py and in the speed controller object
                shares.Kp = float(myUART.read())
                control.speedCtrl.setKp(shares.Kp)
                myUART.write('Input accepted. Kp set to ' + str(shares.Kp) + '.\r\n')
                state = 0
    

    except KeyboardInterrupt:
        # This except block catches "Ctrl-C" from the keyboard to end the
        # while(True) loop when desired
        print('\n\n--- PROGRAM STOP ---')
        print('\nCTRL+C has been pressed.\n')
        control.stop()
        break
"""@file    frontEnd.py
@brief      Front-end for Lab 0xFF, only component run on PC
@details    Communicates with main.py through a serial bus.

Can send and receive strings through the serial bus; this is utilized
by sending characters to control the main.py program and receiving
data after data collection has stopped.

This program also handles the received data and plots the results.
Currently, it is configured to receive velocity and position data
(one set recorded data, one set reference data) and compare the
two sets. A performance metric, J, is used to calculate the 
precision of the motor system.

Note that this program functions as a finite-state machine.
            
<B>Source code:</B> https://bitbucket.org/jfrabosi/me305_labs/src/master/Lab0xFF/
            
@author     Jakob Frabosilio
@date       3/18/2021
"""

import time
import serial
import matplotlib.pyplot as myPlot
import csv

def sendChar():
    ''' Triggers an input command that sends an ASCII character through the serial bus
    @return     Returns the uppercase variant of the inputted character.
    '''
    inv = input('Input: ')
    if inv.upper() != 'H':
        ser.write(str(inv).encode('ascii'))
    return inv.upper()

def cmdsMsg():
    ''' Prints the list of possible inputs
    '''
    print('\nCHARACTER | ACTION')
    print('  G / g   | Run motor and begin data collection (only available when not collecting data)')
    print('  S / s   | Stop motor and end data collection (only available when collecting data)')
    print('  O / o   | Oscillate motor for testing (only available when not collecting data)')
    print('  Z / z   | Zero motor and encoder position')
    print('  P / p   | Get current encoder positon')
    print('  D / d   | Get change from last encoder position')
    print('  X / X   | Set value of Kp')
    print('  H / h   | Show this list again')

# ----- DEFINING CONSTANTS AND PORTS -----

## Serial port used to communicate with Nucleo board
ser = serial.Serial(port='COM8',baudrate=115273,timeout=1)

## State-transition variable
state = 0

## Start time of each input loop (allows time for Nucleo to send data and for it to be received)
startTime = time.time()

## Counter variable
n = 0

## String that holds raw string data sent from Nucleo, from data collection state
csvList = ''

## String containing stripped csvList string data
strippedString = ''

## String containing split and cleaned strippedString data
splitStrings = ''

## Length of each data array (time, position, etc.)
arrLen = 0

## List used to calculate a corrected average velocity value, used in testing (see readme.txt)
vBarList = []

## Sum of differences, used in calculating standard deviation, used in testing (see readme.txt)
sumDiff = 0

## Performance metric
J = 0



# ----- PROGRAM START -----

print('\nHello! To get started, make sure that the Nucleo board is running'
      ' and that the LED is blinking at a moderate pace. Alternatively, open'
      ' the REPL and look for the \'Program Ready!\' message.')

print('\nIf you aren\'t using the REPL through PuTTY, use the LED on the Nucleo'
      ' as an indicator of the program. If the LED flashes at a moderate pace,'
      ' that means you are in the waiting period and data collection is not'
      ' actively happening. If the LED flashes at a fast pace (roughly twice'
      ' as fast as normal), you are in the data collection state and can press'
      ' \'S\' at any point to stop. If the LED stops flashing at some point'
      ' after you started data collection, then data collection has stopped'
      ' and you must press \'S\' to continue. After you press \'S\', make sure'
      ' you look in the \'Plots\' tab to see the output!')

print('\nThis program can be repeated as many times as needed. Inputting \'S\''
      ' will always bring you back to the start of the program. At any point,'
      ' input \'H\' or \'h\' to display the list of possible inputs. Don\'t'
      ' forget to zero the position of the encoder if you run multiple trials!')

print('\nOnce you\'re ready, you can input any of these characters (uppercase'
      ' or lowercase both work) to control the Nucleo board program.')

cmdsMsg()

while True:    
    try:
        
# ----- WAITING FOR G STATE -----

        if state == 0:
            
            # If one second has not elapsed since last input, wait until it has.
            # This allows time for the Nucleo to send serial data over the
            # serial bus and is a UX decision for the user.
            if time.time() - startTime > 1 and ser.in_waiting == 0:
                
                # If character input is 'G', send to the next state
                myChar = sendChar()
                if myChar == 'G':
                    state = 1
                    startTime = time.time() - 1
                elif myChar == 'H':
                    cmdsMsg()
                startTime = time.time()
                
            elif ser.in_waiting != 0:
                print(ser.readline().decode('ascii'))
                
# ----- WAITING FOR S STATE -----                
                
        elif state == 1:
            
            # Almost identical to the previous state. The beauty of the way
            # this program handles state transitions is it only moves when
            # 'G' and 'S' are inputted, meaning the user can zero the position
            # or do other commands without messing up the data collection.

            if time.time() - startTime > 1 and ser.in_waiting == 0:
                
                # Once the user inputs 'S', data will start to be sent over the
                # serial bus from the Nucleo and the program will be sent to 
                # the next state.
                
                myChar = sendChar()
                if myChar == 'S':
                    state = 2
                    startTime = time.time() - 1
                    print(ser.readline().decode('ascii'))
                elif myChar == 'H':
                    cmdsMsg()
                startTime = time.time()
                
            elif ser.in_waiting != 0:
                print(ser.readline().decode('ascii'))
            
# ----- DATA RECEIVING STATE -----            
            
        elif state == 2:
            
            # The program stays in this state until no data is remaining in
            # the serial bus. Every 500ms, the program checks if there is data
            # in the bus. If there is, it adds it to the csvList string.
            # Otherwise, the state transitions to the next state.
            
            if time.time() - startTime > .5:
                if ser.in_waiting != 0:
                    csvList = ser.readline().decode()
                    
                elif ser.in_waiting == 0:
                    state = 3
                    
                startTime - time.time()
        
# ----- DATA CHECK STATE -----
        
        elif state == 3:
            
            # This state strips the csvList string and checks if it is empty.
            # If it's empty, then an error has occured in the Nucleo program.
            # This will likely never be triggered during normal operation, but
            # is extremely helpful in debugging.
            
            strippedString = csvList.strip()
            if strippedString == '':
                state = 0
                print('\nError! Data not received.')
            else:
                state = 4

# ----- DATA SORTING / PLOTTING STATE -----
                
        elif state == 4:
            
            # The data is trimmed and sorted into its respective lists in this
            # state. See readme.txt for a note on the sorting process.
            
            splitStrings = strippedString.split(', ')
            splitStrings[1] = splitStrings[1].replace('[','')
            arrLen = int((len(splitStrings)-1) / 5)
            splitStrings[5*arrLen] = splitStrings[5*arrLen].replace('])','')
            
            times = []
            posRef = []
            posMea = []
            velRef = []
            velMea = []
            
            for n in range(arrLen + 1):
                if n == 0:
                    pass
                elif float(splitStrings[5*n - 4]) < 0.001 and n != 1:
                    break
                else:
                    times.append(float(splitStrings[5*n - 4]))
                    posRef.append(float(splitStrings[5*n - 3]))
                    posMea.append(float(splitStrings[5*n - 2]))
                    velRef.append(float(splitStrings[5*n - 1]))
                    velMea.append(float(splitStrings[5*n]))
                
            # This isn't really used for this lab, but here we write the 
            # collected data to a .CSV file. Just in case, I suppose?
            
            with open('myCSV.csv', 'w', newline='') as data:
                writing = csv.writer(data)
                writing.writerow(times)
                writing.writerow(posRef)
                writing.writerow(posMea)    
                writing.writerow(velRef)
                writing.writerow(velMea)            
                
            # Here, we make two plots: one for velocity, one for position.
            
            myPlot.figure()    
            myPlot.plot(times, posRef, 'k', label='Reference')
            myPlot.plot(times, posMea, 'r--', label='Measured')
            myPlot.xlabel('Time (s)')
            myPlot.ylabel('Position (Degrees)')
            myPlot.title('Position versus Time')
            myPlot.legend(loc='lower right')
            myPlot.show(block=False)
            
            myPlot.figure()
            myPlot.plot(times, velRef, 'k', label='Reference')
            myPlot.plot(times, velMea, 'r--', label='Measured')
            myPlot.xlabel('Time (s)')
            myPlot.ylabel('Speed (RPM)')
            myPlot.title('Speed versus Time')
            myPlot.legend(loc='lower right')
            myPlot.show(block=False)

            state = 0
        
            # Here's where we calculate J, the performance metric. J is calculated
            # as the sum of all (ref_velocity - measured_velocity)^2 +
            # (ref_position - measured_position)^2. For this program, J is very,
            # very big. See the note in the readme.txt file.
            
            for x in range(len(velRef)):
                J += (velRef[x] - velMea[x])**2 + (posRef[x] - posMea[x])**2
                
            J = J/len(velRef)
            print('J = ' + str(int(J)))
            
            strippedString = ''
            
            # The code below was used for calculating an optimal value for Kp.
            # See the note in the readme.txt.
            
            # for x in velMea:
            #     if x > 150 and x < 450:
            #         vBarList.append(x)
            
            # vBar = sum(vBarList) / len(vBarList)
            
            # for x in vBarList:
            #     sumDiff += (x-vBar)**2
                
            # sDev = (sumDiff / (len(vBarList)-1))**0.5
            # error = 100*(vBar - 300) / 300 
            # print('\nvBar = ' + str(int(vBar)) + ' RPM')
            # print('\nError = ' + str(error) + '%')
            # print('\nsDev = ' + str(int(sDev)) + ' RPM')
            # sumDiff = 0
            # vBarList = []
            
    except KeyboardInterrupt:
        # This except block catches "Ctrl-C" from the keyboard to end the
        # while(True) loop when desired
        print('\n\n--- PROGRAM STOP ---')
        print('\nCTRL+C has been pressed.\n')
        ser.close()
        break
    
ser.close()

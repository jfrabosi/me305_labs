"""@file    Frabosilio_ME305_Lab0x03.py
@brief      Morse-code-teaching Simon Says game for Nucleo board.
@details    Selects a random five-letter word from codeSet.py, converts it to
            Morse code, displays the LED pattern for the first letter, reads
            user button presses in real-time, and proceeds to a fail state if
            button presses don't match the LED pattern within a certain margin
            or alternatively moves on to the next letter of the word.
            
            See learnMorse.py for the majority of the code/logic/FSM behind
            this lab.
            
            See codeSet.py for the libraries of Morse code and word lists used.
            
            <B>Source code:</B> https://bitbucket.org/jfrabosi/me305_labs/src/master/Lab0x03/
@author:    Jakob Frabosilio
@date       02/17/2021
"""

# code for running on Anaconda / PuTTY
# ampy --port COM4 run Frabosilio_ME305_Lab0x03.py
# ampy --port COM4 put Frabosilio_ME305_Lab0x03.py Frabosilio_ME305_Lab0x03.py
# ampy --port COM4 put learnMorse.py learnMorse.py
# ampy --port COM4 put wordSet.py wordSet.py

# execfile('Frabosilio_ME305_Lab0x03.py')

import pyb
from learnMorse import learnMorse

if __name__ == "__main__":
    # Program initialization
    
    ## Stores an iteration of the learnMorse class, in order to run it in this program
    programName = learnMorse()
    
    while True:
        try:
            programName.run()
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard to end the
            # while(True) loop when desired
            print('\n\n--- PROGRAM STOP ---')
            print('\nCTRL+C has been pressed.\n')
            pyb.hard_reset()
            break

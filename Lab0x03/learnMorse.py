"""@file    learnMorse.py
@brief      Python class for Frabosilio_ME305_Lab0x03.py - hosts the Morse code game
@details    Read documentation on Frabosilio_ME305_Lab0x03.py for details.
            
            <B>Source code:</B> https://bitbucket.org/jfrabosi/me305_labs/src/master/Lab0x03/
            
@author:    Jakob Frabosilio
@date       02/17/2021
"""

import utime
import pyb
import codeSet

class learnMorse:
    
# ----- STATE CONSTANTS -----
    
    ## State 0: Initial
    S0_INIT = 0
    
    ## State 1: Holding State
    S1_HOLD = 1
    
    ## State 2: Word-Selection State
    S2_WORD = 2
    
    ## State 3: LED State
    S3_LED = 3
    
    ## State 4: Input (Button) State
    S4_INPUT = 4
    
    ## State 5: Fail State
    S5_FAIL = 5
    
    ## State 6: Reassignment State
    S6_REASS = 6
    
 # ----- INITIALIZATION -----   
    
    ## Defines variables for use in program
    def __init__(self, timeStep=0.25, percentOff = .75, dbgFlag=False):
        ## State-tracking variable
        self.state = 0
        
        ## Tracks previous state
        self.lastState = 0
        
        ## Equivalent to one unit; used for Morse code timing
        self.timeStep = timeStep
        
        ## Sets +- percentage for input tracking; 0.75 represents a +-75% margin
        self.percentOff = percentOff
        
        ## Turns on state transition printing
        self.dbgFlag = dbgFlag
        
        ## LD2 LED pin
        self.pinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
        
        ## B1 button pin
        self.pinC13 = pyb.Pin(pyb.Pin.cpu.C13) 
        
        ## Activates callback function when button is pressed
        self.ButtonInt = pyb.ExtInt(self.pinC13, mode=pyb.ExtInt.IRQ_RISING_FALLING,
                                    pull=pyb.Pin.PULL_NONE, callback=self.switch)
        
        ## Tracks if button has undergone full cycle (not pressed -> pressed -> unpressed)
        self.fullSwitch = False
        
        ## Sets fullSwitch; when counter goes up to 2, full cycle has happened
        self.fullCounter = 0
        
        ## Tracks if button changes state (either rising or falling action)
        self.switchFlag = False
        
        ## Randomly selected integer that indexes the wordList
        self.n = 0
        
        ## Current five-letter string
        self.word = ''
        
        ## Morse code translation of current string
        self.code = ''
    
        ## Tracks if user is a dummy and has attempted to hold the button during the LED phase
        self.dummy = False
        
        ## Adds a stopping block once the LED phase ends, preventing state-transition until user input
        self.stopper = True
        
        ## Start time of current unit input/output (button/LED) tracking
        self.startTime = 0        
        
        ## End time of current unit input (button) tracking
        self.endTime = 0
        
        ## Difference between current time and start time, used in LED phase
        self.diff = 0
        
        ## Difference between button unpressing and button pressing, used in input phase
        self.waitDiff = 0
        
        ## Difference between button pressing and button unpressing, used in input phase
        self.pressDiff = 0
        
        ## Current user score
        self.score = 0
        
        ## Current round of game (first letter only -> phase = 1)
        self.phase = 0
        
        ## Current letter, used to track in both input and LED phase
        self.letter = 0
        
        ## Current unit, used to track in both input and LED phase
        self.unit = 0
        
        
    ## Runs the program    
    def run(self):

# ----- INITIALIZATION STATE -----

        if self.state == self.S0_INIT:
            print('\n--- INITIALIZING ---')
            self.pinA5.low()
            print('\nThis program will teach you Morse code by converting a '
                  'string of text into its Morse code equivalent.')
            print('\nThen, the LED pin on your Nucleo board will blink, corresponding '
                  'to the first character of your string of text.') 
            print('\nYou\'ll have to copy the LED flashing by pressing and holding the blue '
                  'B1 button on the board. If you successfully copy the first '
                  'character, '
                  'you\'ll do the same thing again; except, this time, you have '
                  'to watch and copy the first and second character! ' 
                  'This will continue until you\'ve completed the whole string.')
            print('\nFor each round (i.e. new character) you complete, '
                  'you\'ll be awarded one point. For each final round (i.e. '
                  'full five-letter word string) you complete, you\'ll get five '
                  'points. How many points can you get in a row? Good luck!')
            print('\nYour word will be randomly selected from a database of '
                  'approximately 807 five-letter words.')
            print('\nPlease press and release the blue button on your Nucleo '
                  'board twice to continue.')
            print('\nPro-tip: if the program seems like it\'s glitching out '
                  'and not recording proper button presses or is calling you '
                  'a disappointment when you haven\'t done anything wrong, '
                  'there\'s most likely been a tracking issue with your Nucleo '
                  'board\'s button. If this occurs, please press the black button '
                  'on your Nucleo board and restart the program.')
            self.transitionTo(self.S1_HOLD)
            
# ----- HOLDING STATE -----
            
        # This state exists to allow a 'pause' in the program when waiting
        # for user input. When the program moves to this state, it will 
        # appear to do nothing until user input (full button press) occurs.
        # Then, the program will continue to the state it was supposed to go
        # to. For example, the program might go from State 4 to State 1, then
        # go to State 5.
        
        elif self.state == self.S1_HOLD:
            if self.fullSwitch:
                self.transitionTo(self.lastState + 1)
                self.fullSwitch = False
                self.switchFlag = False
            self.startTime = utime.ticks_ms()
                
# ----- WORD-SELECTION STATE -----
            
        # This state generates a random integer between 0 and 806 and chooses 
        # a list of 807 five-letter words. Then, it converts the word into
        # Morse code and initializes the gameplay loop.
        
        elif self.state == self.S2_WORD:
            print('\n\n--- NEW STRING ---')
            self.n = pyb.rng() % 806
            self.word = codeSet.wordSet[self.n]
            if self.dbgFlag:
                self.word = 'bella'
            self.code = codeSet.convertToMorseCode(self.word)
            print('\nYour word is \'' + str(self.word) + '\'.')
            if self.dbgFlag:
                print('The morse code output is:')
                print(''.join(self.code))
            print('\nGet ready! Once you press and release the blue button on '
                  'the board, the first letter sequence will begin. You will '
                  'not be able to activate your button while the LED is flashing. '
                  '\n\nOnce the LED stops flashing, you can begin to repeat the '
                  'pattern using your button presses. You must match the morse '
                  'code both in pattern and in tempo - going too slow or too '
                  'fast will cause you to fail.')
            print('\nPlease press and release the blue button on your Nucleo '
                  'board to continue.')
            
            # Here, we'll set the tracking variables (phase, letter, unit)
            # to their initial values. This step is key; otherwise, the game
            # could start in the incorrect position.
            
            self.phase = 1
            self.letter = 0
            self.unit = 0
            self.transitionTo(self.S1_HOLD)
           
# ----- LED STATE -----

        # In this state, the Morse code string is converted into LED pulses.
        # The pulses follow Morse code convention, where a dot is one unit,
        # a dash is three units, a space between dots/dashes is one unit, and
        # a space between letters is three units. The length of one unit is
        # defined by the variable timeStep, in seconds. 
        
        # The state knows how many rounds (from henceforce, 'phases') have taken
        # place so far. In the beginning, it will be the first phase, and only
        # the first letter's Morse code pattern will be displayed.
        
        # For consistency, a 'unit' is an individual dot/dash/space, a 'letter'
        # is one letter from the word string, and a 'phase' is the current round
        # (of which there are five total per word).
        
        elif self.state == self.S3_LED:
            
            # Here, we calculate the time difference between the start of each unit
            # and the current time. This value is in seconds.
            
            self.diff = utime.ticks_diff(utime.ticks_ms(), self.startTime) / 1000

            # First, we check if all of the letters for the current phase have
            # been displayed - if the letter number equals the phase number,
            # we have reached the limit for this phase.
            
            if self.letter == self.phase:
                
                # If the user decides to hold the button down while the LED
                # is flashing (which tends to break my code), I will verbally
                # berate them and force them to unpress the button.
                
                if self.switchFlag and not self.fullSwitch and not self.dummy and self.stopper:
                    print('\nLet go of the button. You are a disappointment.')
                    self.dummy = True
                
                # Once the user has unpressed the button (or immediately after
                # the last letter flashes for this phase), we'll print a message
                # explaining what to do. This also allows the user to wait as
                # long as they want after the LED pattern flashes.
                
                elif (self.switchFlag and self.fullSwitch) or not self.switchFlag:
                    if self.stopper:
                        print('\nNow, repeat the pattern you just saw flash by pressing '
                              'and holding the blue button for the correct intervals.')
                        if self.phase == 1:
                            print('\nIf you mess up, the LED will turn on and '
                                  'an error message will give you instructions. Please '
                                  'wait for three seconds after you fail and trigger the LED - '
                                  'any button presses in that time period will not be '
                                  'registered to avoid input error.')
                        self.switchFlag = False
                        self.fullSwitch = False
                        self.stopper = False
                        
                # Once the user depresses the button, the timer starts ticking
                # and we head into the Input state. We'll make sure to reset 
                # any variables that we might have adjusted during the LED state,
                # especially the tracking variables 'unit' and 'letter'. These 
                # variables are important for tracking in both the LED and the
                # Input states, and must be reset when swapping between the two.
                
                elif self.switchFlag and not self.stopper:
                        self.dummy = False
                        self.startTime = utime.ticks_ms()
                        self.switchFlag = False
                        self.unit = 0
                        self.letter = 0
                        self.transitionTo(self.S4_INPUT)
                        self.stopper = True
            
            # If the current unit is a dot and is the last unit of the current
            # phase, then don't rest for the last count.
            
            elif (self.code[self.unit] == ".") and (self.code[self.unit+1] == " ") \
                 and self.letter == self.phase-1:
                if self.diff < self.timeStep:
                    self.pinA5.high()
                elif self.diff > self.timeStep:
                    self.pinA5.low()
                    self.unit += 1
                    self.letter += 1
                    self.startTime = utime.ticks_ms()
            
            # If the current unit of the Morse code string is a dot, then the
            # LED will flash for the appropriate amount of time, and then will
            # proceed to the next unit after it has flashed and waited.
            
            elif self.code[self.unit] == ".":
                if self.diff < self.timeStep:
                    self.pinA5.high()
                elif self.diff > self.timeStep and self.diff < 2*self.timeStep:
                    self.pinA5.low()
                elif self.diff > 2*self.timeStep:
                    self.unit += 1
                    self.startTime = utime.ticks_ms()
                    
            # See above note: same thing for dash.
            
            elif (self.code[self.unit] == "-") and (self.code[self.unit+1] == " ") \
                 and self.letter == self.phase-1:
                if self.diff < 3*self.timeStep:
                    self.pinA5.high()
                elif self.diff > 3*self.timeStep:
                    self.pinA5.low()
                    self.unit += 1
                    self.letter += 1
                    self.startTime = utime.ticks_ms()
            
            elif self.code[self.unit] == "-":
                if self.diff < 3*self.timeStep:
                    self.pinA5.high()
                elif self.diff > 3*self.timeStep and self.diff < 4*self.timeStep:
                    self.pinA5.low()
                elif self.diff > 4*self.timeStep:
                    self.unit += 1
                    self.startTime = utime.ticks_ms()
                    
            # See above note: same thing for space, except we don't flash here,
            # and the letter count increases here too.
            
            elif self.code[self.unit] == " ":
                if self.diff < 2*self.timeStep:
                    self.pinA5.low()
                else:
                    self.unit += 1
                    self.letter += 1
                    self.startTime = utime.ticks_ms()
            
# ----- INPUT (BUTTON) STATE -----
            
        # This is by far the most complicated state in this entire code, so
        # please parse it carefully.
        
        # There's a few different possibilities for the units now. Before, all
        # we had to worry about was if the unit was a dot, a dash, or a space.
        # But because of the way this state is set up (spaces are inherently
        # linked to dots/dashes for ease of checking inputs), we have to check
        # if there's a space after each unit. So, the possibilities are:
            # Dot followed by space, last letter in current phase
            # Dash followed by space, last letter in current phase
            # Dot followed by space, not last letter in current phase
            # Dash followed by space, not last letter in current phase
            # Dot not followed by space
            # Dash not followed by space
            
        # Basically, how this section works is we can track how long the button
        # has been held for (most recently), and we can track how long the
        # button has been sitting for (un-pressed, most recently). By carefully
        # handling these values and the nested if;else statements, we've created
        # a system where the program knows what unit it's on, and it checks the
        # user's input (after completing a full cycle of pressing -> holding ->
        # unpressing -> waiting) against the expected amount of time it should
        # take to perform that action. There's a fair margin of error (normally
        # +- 75%), but I think I've found the sweet spot with it.
        
        elif self.state == self.S4_INPUT:
            
            # First, we check if we've had a button input. We'll either have
            # completed a full cycle (meaning button was most recently
            # unpressed / rising action was triggered), or pressed down the
            # button (when switchFlag is triggerd but the full cycle hasn't
            # been completed yet).
            
            if self.fullSwitch:
                
                # When the rising action occurs, the time spent holding the
                # button (pressDiff) is calculated.
                
                self.endTime = utime.ticks_ms()
                self.pressDiff = utime.ticks_diff(self.endTime, self.startTime)/1000
                self.fullSwitch = False
                self.switchFlag = False
            
            elif self.switchFlag and not self.fullSwitch:
                
                # When the falling action occurs, the time spent waiting after
                # last releasing the button (waitDiff) is calculated.
                
                self.startTime = utime.ticks_ms()
                self.waitDiff = utime.ticks_diff(self.startTime, self.endTime)/1000
                self.switchFlag = False
                
            # Then, we check what type of unit (or combination of units) we're
            # expecting. We'll first check to see if we've successfully copied
            # the Morse code for the current phase.
            
            if self.letter == self.phase:
                
                # If we're in phase 5 (the last round) and we reach this point
                # in the code, then we win and five points are awarded.
                
                if self.phase == 5:
                    self.score += 5
                    print('\n--- WIN! ---')
                    print('\nCongratulations! You\'ve successfully inputted '
                          'the word ' + str(self.word) + ' in Morse code!')
                    print('\nScore: ' + str(self.score))
                    print('\nIf you\'d like to play again, press the blue '
                          'button. Otherwise, press CTRL+C to exit, and then '
                          'press the black button on the Nucleo board to reset '
                          'the program.')
                    self.waitDiff = 0
                    self.startDiff = 0
                    self.unit = 0
                    self.letter = 0
                    self.state = 1
                    self.transitionTo(self.S1_HOLD)
                    
                # Otherwise, we'll reset all of the variables we changed in 
                # this phase and continue onto the next phase. We'll loop 
                # back to the LED state and the next letter will be added to
                # the Morse code pattern.
                
                else: 
                    self.phase += 1
                    self.score += 1
                    print('\n\n--- SUCCESSFUL INPUT ---')
                    print('\nScore: ' + str(self.score))
                    self.unit = 0
                    self.letter = 0
                    self.startTime = utime.ticks_ms()
                    self.waitDiff = 0
                    self.startDiff = 0
                    print('\nNice job! You correctly inputted the Morse code '
                          'pattern shown. Press the blue button to display '
                          'the pattern again, this time with an additional '
                          'letter.')
                    self.state = 2
                    self.transitionTo(self.S1_HOLD)
                    
            # If we haven't completed the phase, then we'll check each of the
            # potential unit combinations. First, we'll check if we have a
            # dot followed by a space and if we are on the last letter in 
            # this phase.
            
            elif (self.code[self.unit] == ".") and (self.code[self.unit+1] == ' ') \
                 and (self.letter == self.phase-1) and self.pressDiff:
                if self.dbgFlag:
                     print('END DOT CHECK')
                
                # For this unit combination, we only care about the time that
                # the button was held for (henceforth, 'held time'). The button
                # should have been held down for one unit of time.
                
                if self.pressDiff < self.timeStep*(1+self.percentOff):
                    if self.pressDiff > self.timeStep*(1-self.percentOff):
                        self.waitDiff = 0
                        self.pressDiff = 0
                        self.letter += 1
                    
                    # If the held time is less than the margin of error, the
                    # user didn't hold long enough.
                    
                    else:
                        print('\n\n--- FAIL ---\n\nDidn\'t hold long enough!')
                        self.transitionTo(self.S5_FAIL)
                        self.startTime = utime.ticks_ms()
                
                # If the held time is more than the margin of error, the user
                # held for too long.
                
                else:
                    print('\n\n--- FAIL ---\n\nHeld too long!')
                    self.transitionTo(self.S5_FAIL)
                    self.startTime = utime.ticks_ms()
                    
                # The failure conditions are very similar for waiting too long
                # / not long enough, and so I will not comment on those. Any
                # failure sends the user to the Fail state, where any important
                # / code-breaking variables are reset and the user is given
                # a choice to retry the word or get a new word.
            
            # If the unit is a dot but is not the last letter for the current
            # phase, then the below code is run. First, the program checks if
            # the held time is within the margin of error.
            
            # Additionally, note that none of the non-last-letter checks can
            # run unless the wait time (time between the last rising action and
            # the last falling action) is non-zero. Both the wait time and the
            # held time are reset once a unit check occurs - this ensures that
            # the check only occurs once both the wait time and the held time
            # are non-zero (aka, either when someone is in the middle of 
            # holding the button or is in the middle of waiting to press the
            # button again). It's quite clever, if I do say so myself.
            
            elif self.code[self.unit] == "." and self.waitDiff:
                if self.pressDiff < self.timeStep*(1+self.percentOff):
                    if self.pressDiff > self.timeStep*(1-self.percentOff):
                        
                        # If the held time test passes, then we can check whether
                        # the current unit is followed by a space or not. If the
                        # unit is followed by a space, then there will need to
                        # be more time spent waiting. Additionally, if there is
                        # a space, then we need to increase the unit counter by
                        # 2 instead of 1.
                        
                        if self.code[self.unit+1] == ' ':
                            if self.dbgFlag:
                                print('DOT SPACE CHECK')
                                
                            if self.waitDiff < self.timeStep*3*(1+self.percentOff):
                                if self.waitDiff > self.timeStep*3*(1-self.percentOff):
                                    self.unit += 2
                                    self.letter += 1
                                    self.waitDiff = 0
                                    self.pressDiff = 0
                                else:
                                    print('\n\n--- FAIL ---\n\nDidn\'t wait long enough!')
                                    self.transitionTo(self.S5_FAIL)
                                    self.startTime = utime.ticks_ms()
                            else:
                                print('\n\n--- FAIL ---\n\nWaited too long!')
                                self.transitionTo(self.S5_FAIL)
                                self.startTime = utime.ticks_ms()
                                
                        # If there isn't a space directly after the unit, then
                        # we will only check for one unit of time spent waiting,
                        # and the unit counter will only increase by 1.
                        
                        else:
                            if self.dbgFlag:
                                 print('DOT CHECK')
                                 
                            if self.waitDiff < self.timeStep*(1+self.percentOff):
                                if self.waitDiff > self.timeStep*(1-self.percentOff):
                                    self.unit += 1
                                    self.waitDiff = 0
                                    self.pressDiff = 0
                                else:
                                    print('\n\n--- FAIL ---\n\nDidn\'t wait long enough!')
                                    self.transitionTo(self.S5_FAIL)
                                    self.startTime = utime.ticks_ms()
                            else:
                                print('\n\n--- FAIL ---\n\nWaited too long!')
                                self.transitionTo(self.S5_FAIL)
                                self.startTime = utime.ticks_ms()
                    else:
                        print('\n\n--- FAIL ---\n\nDidn\'t hold long enough!')
                        self.transitionTo(self.S5_FAIL)
                        self.startTime = utime.ticks_ms()
                else:
                    print('\n\n--- FAIL ---\n\nHeld too long!')
                    self.transitionTo(self.S5_FAIL)
                    self.startTime = utime.ticks_ms()
            
            # The two sections below are essentially an identical copy to the
            # two sections above, except the held time is now three times as long.
            
            elif (self.code[self.unit] == "-") and (self.code[self.unit+1] == ' ') \
                 and (self.letter == self.phase-1) and self.pressDiff:
                if self.dbgFlag:
                    print('END DASH CHECK')
                    
                if self.pressDiff < self.timeStep*3*(1+self.percentOff):
                    if self.pressDiff > self.timeStep*3*(1-self.percentOff):    
                        self.waitDiff = 0
                        self.pressDiff = 0
                        self.letter += 1
                    else:
                        print('\n\n--- FAIL ---\n\nDidn\'t hold long enough!')
                        self.transitionTo(self.S5_FAIL)
                        self.startTime = utime.ticks_ms()
                else:
                    print('\n\n--- FAIL ---\n\nHeld too long!')
                    self.transitionTo(self.S5_FAIL)
                    self.startTime = utime.ticks_ms()
                
            elif self.code[self.unit] == "-" and self.waitDiff:
                if self.pressDiff < self.timeStep*3*(1+self.percentOff):
                    if self.pressDiff > self.timeStep*3*(1-self.percentOff):
                        
                        if self.code[self.unit+1] == ' ':
                            if self.dbgFlag:
                                print('DASH SPACE CHECK')
                                
                            if self.waitDiff < self.timeStep*3*(1+self.percentOff):
                                if self.waitDiff > self.timeStep*3*(1-self.percentOff):
                                    self.unit += 2
                                    self.letter += 1
                                    self.waitDiff = 0
                                    self.pressDiff = 0
                                else:
                                    print('\n\n--- FAIL ---\n\nDidn\'t wait long enough!')
                                    self.transitionTo(self.S5_FAIL)
                                    self.startTime = utime.ticks_ms()
                            else:
                                print('\n\n--- FAIL ---\n\nWaited too long!')
                                self.transitionTo(self.S5_FAIL)
                                self.startTime = utime.ticks_ms()
                        else:
                            if self.dbgFlag:
                                 print('DASH CHECK')
                                 
                            if self.waitDiff < self.timeStep*(1+self.percentOff):
                                if self.waitDiff > self.timeStep*(1-self.percentOff):
                                    self.unit += 1
                                    self.waitDiff = 0
                                    self.pressDiff = 0
                                else:
                                    print('\n\n--- FAIL ---\n\nDidn\'t wait long enough!')
                                    self.transitionTo(self.S5_FAIL)
                                    self.startTime = utime.ticks_ms()
                            else:
                                print('\n\n--- FAIL ---\n\nWaited too long!')
                                self.transitionTo(self.S5_FAIL)
                                self.startTime = utime.ticks_ms()
                    else:
                        print('\n\n--- FAIL ---\n\nDidn\'t hold long enough!')
                        self.transitionTo(self.S5_FAIL)
                        self.startTime = utime.ticks_ms()
                else:
                    print('\n\n--- FAIL ---\n\nHeld too long!')
                    self.transitionTo(self.S5_FAIL)
                    self.startTime = utime.ticks_ms()
        
# ----- FAIL HOLDING STATE -----   
        
        # We can't all be winners, can we? 
        
        # In this state, we reset any of the variables that have the potential
        # to wreck the program. This state has caused the majority of the 
        # problems when debugging this code. Who would have thought?
        
        # Also, the user is given an option to retry the last word from the
        # beginning, or to try a new word. A short/long button press (after
        # a short waiting period to prevent misclicks when going from the 
        # normal game to the Fail state) allows the user to make this choice.
        
        # Also also, the LED on the board will now flash rapidly when an error
        # occurs, in an attempt to notify the user.

        elif self.state == self.S5_FAIL: 
            if utime.ticks_diff(utime.ticks_ms(), self.startTime) > 2500:
                print('\nWould you like to try that word again? If so, tap the '
                      'button once.')
                print('\nIf you would like a new word, hold the button for at '
                      'least one second.')
                print('\nOtherwise, if you would like to quit, press CTRL+C to '
                      'exit, and then press the black button on the Nucleo '
                      'board to reset the program.')
                print('\nScore: ' + str(self.score))
                self.pressDiff = 0
                self.fullSwitch = False
                self.switchFlag = False
                self.transitionTo(self.S6_REASS)
                
            self.pinA5.high()
            
            if self.switchFlag and not self.fullSwitch:
                print('\nPlease wait three seconds before pressing the button '
                      'after failing.')
                self.switchFlag = False
                self.startTime = utime.ticks_ms()
                self.fullSwitch = False
            
            self.fullSwitch = False

# ----- REASSIGNMENT STATE -----                
                
        elif self.state == self.S6_REASS:
            
            self.pinA5.low()
            
            if self.fullSwitch:
                self.endTime = utime.ticks_ms()
                self.pressDiff = utime.ticks_diff(self.endTime, self.startTime)/1000
                self.fullSwitch = False
                self.switchFlag = False
            
            elif self.switchFlag and not self.fullSwitch:
                self.startTime = utime.ticks_ms()
                self.switchFlag = False
                
            if self.pressDiff > 1:
                self.unit = 0
                self.letter = 0
                self.phase = 0
                self.pressDiff = 0
                self.waitDiff = 0
                self.transitionTo(self.S2_WORD)
                
            elif self.pressDiff < 1 and self.pressDiff > 0:
                self.state = 2
                print('\n\n--- REATTEMPTING STRING ---')
                print('\nYour word is \'' + str(self.word) + '\'.')
                if self.dbgFlag:
                    print('The morse code output is:')
                    print(''.join(self.code))
                print('\nGet ready! Once you press and release the blue button on '
                      'the board, the first letter sequence will begin. You will '
                      'not be able to activate your button while the LED is flashing. '
                      '\n\nOnce the LED stops flashing, you can begin to repeat the '
                      'pattern using your button presses. You must match the morse '
                      'code both in pattern and in tempo - going too slow or too '
                      'fast will cause you to fail.')
                print('\nPlease press and release the blue button on your Nucleo '
                      'board to continue.')
                self.unit = 0
                self.letter = 0
                self.phase = 1
                self.pressDiff = 0
                self.waitDiff = 0
                self.transitionTo(self.S1_HOLD)
            
            elif self.pressDiff < 0:
                print('error!')
            
# ----- FUNCTIONS -----
            
    def transitionTo(self, newState):
        '''
        @brief          Transitions FSM to chosen state
        @param newState New chosen state
        @return         None; if dbgFlag, prints state transition
        '''
        if self.dbgFlag:
            print(str(self.state) + "->" + str(newState))
        self.lastState = self.state
        self.state = newState
    
    def switch(self, IRQ_src):
        '''
        @brief          Toggles switchFlag and fullSwitch, depending on current state
        @param IRQ_sec  Button press input
        @return         Sets switchFlag to True, sets fullSwitch to True if
                        switchFlag has triggered on previous press (should
                        activate when button is released)
        '''
        self.fullCounter += 1
        if self.fullCounter == 2:
            self.fullCounter = 0
            self.fullSwitch = True
            if self.dbgFlag:
                print('up')
        else:
            if self.dbgFlag:
                print('down')
        self.switchFlag = True
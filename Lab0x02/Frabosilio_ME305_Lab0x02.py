"""@file    Frabosilio_ME305_Lab0x02.py
@brief      Nucleo board program that cycles between five LED pulse patterns.
@details    Cycles through five different LED pulse patterns on a Nucleo-L476RG:
            a square wave, a sine wave, a saw wave, a heartbeat pulse, and an
            SOS pulse. Only the first three waves are part of the lab assignment;
            the other two were just for fun. 
            
            Takes a user input from button B1 and outputs the LED pulse pattern
            on LED LD2.
            
            Source code: https://bitbucket.org/jfrabosi/me305_labs/src/master/Lab0x02/
@image      html Lab0x02_FSM2.jpg "Finite-State Machine Diagram"
            
@author:    Jakob Frabosilio
@date       1/28/2021
"""

# code for running on Anaconda / PuTTY
# ampy --port COM4 run Frabosilio_ME305_Lab0x02.py
# ampy --port COM4 put Frabosilio_ME305_Lab0x02.py Frabosilio_ME305_Lab0x02.py
# execfile('Frabosilio_ME305_Lab0x02.py')

import pyb
import math
import utime

def switch(IRQ_src):
    '''
    @brief          Toggles switchFlag to True when button is pressed
    @param IRQ_sec  Button press input
    @return         Sets switchFlag to True
    '''
    global switchFlag
    switchFlag = True

# Main program begin
if __name__ == "__main__":
    # Program initialization
    
    print('\n--- Press CTRL+D to end the program at any time. ---')
    print('\nWelcome! This program will cycle the LD2 LED pin on your Nucleo-L476RG '
          'board. \n\nBy pressing the blue button (B1) on your Nucleo, you '
          'can switch between the five LED pulse patterns: a square wave, a sine '
          'wave, a saw wave, a heartbeat pulse, and an SOS pulse. \n\nEnjoy!')
    
    ## Turns true if button is pressed, is reset immediately after
    switchFlag = False
    
    ## LD2 LED pin
    pinA5 = pyb.Pin(pyb.Pin.cpu.A5)
    
    ## Timer object for PWM
    tim2 = pyb.Timer(2, freq = 20000)
    
    ## PWM controller object
    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
    
    ## B1 button pin
    pinC13 = pyb.Pin(pyb.Pin.cpu.C13) 
    
    ## Activates callback function when button is pressed
    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,
                           pull=pyb.Pin.PULL_NONE, callback=switch)
     
    ## State position tracker
    state = 0
    
    ## Time difference between start of state and current time
    diff = 0
    
    ## Initial time for most recent iteration of State 1
    time_1 = 0

    ## Initial time for most recent iteration of State 2  
    time_2 = 0
    
    ## Initial time for most recent iteration of State 3    
    time_3 = 0
    
    ## Initial time for most recent iteration of State 4
    time_4 = 0

    ## Initial time for most recent iteration of State 5
    time_5 = 0
    
    ## Duty cycle value (0:100) for current time
    duty = 0
    
    while True:
        try:
            if state == 0:
                # Initial state
                # Turn off LED
                t2ch1.pulse_width_percent(0) 
                
                if switchFlag == True:
                    state = 1
                    switchFlag = False
                    print('\nEntering State 1: Square Wave')
                    # Sets initial time for State 1
                    time_1 = utime.ticks_ms()
                
            elif state == 1:
                # Square wave function
                # Calculate difference between current time and initial time 
                # for this state
                diff = utime.ticks_diff(utime.ticks_ms(),time_1)
                # diff % 1000 makes sure that we only count up to 999 and loop
                # each time we go over; integer division (//) means that
                # for diff > 500, the output of ((diff % 1000) // 500) is 0,
                # and for diff <= 500, the output is 1. This translates into
                # an output of 0 for the first 500ms and 1 for the last 500ms
                # of each loop. Then, subtract the output from 1 to get the
                # opposite (since we want the LED to be on for the first half),
                # and multiply by 100 to get an on-off blinker with a loop
                # every 1000ms or 1s.
                duty = 100*(1-(diff % 1000) // 500)
                t2ch1.pulse_width_percent(duty)
                
                if switchFlag == True:
                    state = 2
                    switchFlag = False
                    print('\nEntering State 2: Sine Wave')
                    time_2 = utime.ticks_ms()

            elif state == 2:
                # Sine wave function
                diff = utime.ticks_diff(utime.ticks_ms(),time_2)
                # (diff/10000) % 1 converts the difference from milliseconds
                # to seconds*10^1, and then counts up to 0.9999 before looping
                # again. Multiplying this by the scalar 2*pi and taking the
                # sine of that product results in an output that goes from
                # 0 to 1 to 0 to -1 to 0 in a sine wave, which is the structure
                # that our desired sine wave has over a period of 10s.
                duty = 50 + 50 * math.sin(math.pi*2*((diff/10000) % 1))
                t2ch1.pulse_width_percent(duty)
                
                if switchFlag == True:
                    state = 3
                    switchFlag = False
                    print('\nEntering State 3: Saw Wave')
                    time_3 = utime.ticks_ms()

            elif state == 3:
                # Saw wave function
                diff = utime.ticks_diff(utime.ticks_ms(),time_3)
                # Similar to the last wave, we convert the difference from
                # ms to s and count up to 0.999 before looping. By scaling the
                # output by 100, we end up with a linear increase from 0 to 100
                # which results in a saw wave over a period of 1s.
                duty = 100*((diff/1000) % 1)
                t2ch1.pulse_width_percent(duty) 
                
                if switchFlag == True:
                    state = 4
                    switchFlag = False
                    print('\nEntering State 4: Heartbeat')
                    time_4 = utime.ticks_ms()
                    
            elif state == 4:
                # Heartbeat function
                diff = utime.ticks_diff(utime.ticks_ms(),time_4)
                # For first half of the 1s period, the LED pulses twice.
                if (diff % 1000) < 500:
                    duty = 100*(1 - ((diff % 250) // 125))
                # For the second half, the LED turns off.
                else:
                    duty = 0                       
                t2ch1.pulse_width_percent(duty) 
                
                if switchFlag == True:
                    state = 5
                    switchFlag = False
                    print('\nEntering State 5: SOS') 
                    time_5 = utime.ticks_ms()
                    
            elif state == 5:
                # SOS function
                diff = utime.ticks_diff(utime.ticks_ms(),time_5)
                # If you've gotten this far, you probably understand this code.
                if (diff % 5000) < 750:
                    duty = 100*(1 - ((diff % 250) // 125))
                elif (diff % 5000) < 1250 and (diff % 5000) >= 750:
                    duty = 0
                elif (diff % 5000) < 2750 and (diff % 5000) >= 1250:
                    duty = 100*((diff % 500) // 250)
                elif (diff % 5000) < 3250 and (diff % 5000) >= 2750:
                    duty = 0
                elif (diff % 5000) < 4000 and (diff % 5000) >= 3250:
                    duty = 100*(1 - ((diff % 250) // 125))
                elif (diff % 5000) >= 4000:
                    duty = 0    
                t2ch1.pulse_width_percent(duty) 
                                
                if switchFlag == True:
                    state = 1
                    switchFlag = False
                    print('\nEntering State 1: Square Wave')  
                    time_1 = utime.ticks_ms()   
                    
        except KeyboardInterrupt:
            # ctrl+c breaks while() loop
            break
